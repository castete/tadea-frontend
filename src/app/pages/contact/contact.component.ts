import { Component, OnInit } from '@angular/core';
import { EmailServiceService } from 'src/app/pages/services/email-service.service';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
declare var $: any;
declare var bootstrap: any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  form:FormGroup;
  alert=false;
  constructor(private _email:EmailServiceService) {
    this.form = new FormGroup({
      nombres : new FormControl('', Validators.required),
      apellidos: new FormControl('', Validators.required),
      email : new FormControl('', Validators.required),
      body : new FormControl('', Validators.required),
    })

  }

  ngOnInit(): void {
  }

  enviar(){

    // this.form.controls['nombres'].setValue('');
    // this.form.controls['apellidos'].setValue('');
    // this.form.controls['email'].setValue('');
    // this.form.controls['body'].setValue('');

    let enviar = this.form.value;
    this._email.sendMessage(enviar).subscribe(
      data=> {
        console.log(data);
        this.alert = true;
        let reset = {
          nombres : "",
          apellidos : "",
          email : "",
          body : "",
        }
        this.form.setValue(reset);
      },
      err=> { console.log(err);}
    )

    
  }

  cerrar(){
    this.alert = false;
  }

}
