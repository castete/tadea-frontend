import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import servicios  from "src/assets/json/servicios.json";

@Component({
  selector: 'app-detail-service',
  templateUrl: './detail-service.component.html',
  styleUrls: ['./detail-service.component.scss']
})
export class DetailServiceComponent implements OnInit {

  servicio:Array<any>= servicios;
  serv:any;

  constructor(private route: ActivatedRoute ) {
    // console.log(this.route.snapshot.params.id);
   }

  ngOnInit(): void {
    this.serv = this.servicio.find(x => x.id == this.route.snapshot.params.id );
  }
}
