import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmailServiceService {

  constructor( private http:HttpClient) { }

  sendMessage(data:any):Observable<any>{
    return this.http.post('http://localhost:8001/api/servicio',data);
  }
}
