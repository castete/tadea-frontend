import { Component, OnInit } from '@angular/core';
import reconocimientos  from "src/assets/json/reconocimientos.json";


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  open=false;
  reconocimientos=reconocimientos;
  recSelect={
    id: '',
    nombre: '',
    descripcion: '',
    img: '',
    pais: '',
    ciudad: '',
    anio: '',
    fecha: '',
  };
  constructor() { }

  ngOnInit(): void {
  }

  openModal(row:any){
    console.log('este');
    this.open = true;
    this.recSelect = row;
  }

  closeModal(){
    this.open = false;
  }

}
