import { Component, OnInit } from '@angular/core';
import servicios  from "src/assets/json/servicios.json";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  servicios :any = servicios;

  constructor() {
    console.log(this.servicios);
  }

  ngOnInit(): void {
  }

}
