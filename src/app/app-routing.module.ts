import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { DetailServiceComponent } from './pages/detail-service/detail-service.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: 'inicio', component: HomeComponent},
  { path: 'detalle/:id', component: DetailServiceComponent},
  { path: 'contactanos', component: ContactComponent},
  { path: 'sobre-nosotros', component: AboutComponent},
  {path: '', redirectTo:'/inicio', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
